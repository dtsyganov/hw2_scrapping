# Курс: Python для продвинутых специалистов

## Домашнее задание по теме "Веб-скрапинг" (текст задания *hw_scraping.html*)

Файл 'db.sqlite' в качестве БД с собранными данными

Данные собирались с сайтов "Холодильник" и "Нотик", интересновали ноутбуки в диапазоне 30-80 тысяч рублей, соответственно изначальные ссылки для сбора данных были:
 - https://www.holodilnik.ru/digital_tech/notebook/?ef=price=30000-80000
 - https://www.notik.ru/index/notebooks.htm?srch=true&full=&f305=30000-80000

### Выбранные для оценки параметры и их веса:
 - Цена (руб): -0.005
 - ОЗУ (Мб): 0,03
 - Накопитель (Гб): 0,2

### Запуск программы:

**python3 main.py**

### Топ 5 записей из получившейся таблицы:
(120.29000000000002, 'Ноутбук Acer Aspire 3 A315-57G', 8, 2000, 55990, 'https://goods/notebooks-acer-aspire-3-a315-57g-73f1-black-91236.htm')
(34.97, 'Ноутбук Lenovo IP3 15ALC6 серый (82KU009XRK)', 4, 1024, 33990, 'https://www.holodilnik.ru/digital_tech/notebook/lenovo/ip3_15alc6_seryy_82ku009xrk/')
(4.969999999999999, "Ноутбук Lenovo 15.6'' IPS FHD Lenovo IdeaPad 3 (82HL005VRK) grey", 4, 1024, 39990, 'https://www.holodilnik.ru/digital_tech/notebook/lenovo/82hl005vrk/')
(4.625, 'Ноутбук Lenovo V15 G2 ALC 82KD0031RU черный', 4, 1000, 39099, 'https://www.holodilnik.ru/digital_tech/notebook/lenovo/v15_g2_alc_82kd0031ru_chernyy/')
(-14.909999999999997, 'Ноутбук ACER A315-23-R7LH (NX.HVTER.00N) черный', 8, 1024, 43990, 'https://www.holodilnik.ru/digital_tech/notebook/acer/a315_23_r7lh_nx_hvter_00n_chernyy/')

