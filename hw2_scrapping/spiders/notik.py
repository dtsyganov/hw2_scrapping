import scrapy
import re

class NotikSpider(scrapy.Spider):
  name = 'notik'
  allowed_domains = ['www.notik.ru']
  start_urls = ['https://www.notik.ru/index/notebooks.htm?srch=true&full=&f305=30000-80000']
  # For tests
  #allowed_domains = []
  #start_urls = ['file:///mnt/tmpfs/1.html']

  def parse(self, response):
    for tr in response.xpath("//table[@class='goods-list-grouped-table']/tr"):
      if tr.attrib['class'] == 'goods-list-header':
        compname = tr.xpath(".//h3[@class='glh-title']/span").css("::text").get()
        compurl = 'https:/' + tr.xpath(".//div/div[@class='glh-picture']/a").attrib['href']
      elif tr.attrib['class'] == 'goods-list-table':
        card_selector = tr.xpath(".//td[@class='glt-cell gltc-cart']/a")
        compprice = int(card_selector.attrib.get("ecprice"))
        param_selector = tr.xpath(".//td[@class='glt-cell w4']")[1]
        param = param_selector.css("::text").getall()
        compram = int(re.findall(r'\d+', param[0])[0])
        compstorage = int(re.findall(r'\d+', param[6])[0])
        yield {"name" : compname, "url" : compurl, "price" : compprice, "ram" : compram, "storage": compstorage}

    next_pages = response.css('div.paginator.pright a:not(.active)')
    yield from response.follow_all(next_pages, self.parse)
