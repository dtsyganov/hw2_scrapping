import scrapy
import re

class HolodilnikSpider(scrapy.Spider):
  name = 'holodilnik'
  allowed_domains = ['www.holodilnik.ru']
  start_urls = ['https://www.holodilnik.ru/digital_tech/notebook/?ef=price=30000-80000']
  # For tests
  #allowed_domains = []
  #start_urls = ['file:///mnt/tmpfs/2.html']

  def parse(self, response):
    for comp in response.xpath("//div[@class='col product-specification']"):
      compname = comp.xpath(".//div[@class='product-name']/a/span").css("::text").get()
      compurl = 'https:' + comp.xpath(".//div[@class='product-name']/a").attrib['href']
      compprice = int(comp.xpath(".//div/div[@class='item-order']/meta[@itemprop='price']/@content").get())
      # На всякий случай если емкость накопителя не указана, то считаю ее нулевой
      compram = 0
      compstorage = 0
      for comp_param in comp.xpath(".//div[@class='product-tth']/table/tr"):
        if (comp_param.xpath(".//td[1]/span").css("::text").get() == "Размер оперативной памяти (Гб):"):
          compram = int(comp_param.xpath(".//td[2]/span").css("::text").get())
        elif (comp_param.xpath(".//td[1]/span").css("::text").get() == "Объем жесткого диска (Гб):"):
          # В случае если 2 диска и указано 1024+256, берем первое число
          compstorage = int(re.findall(r'\d+', comp_param.xpath(".//td[2]/span").css("::text").get())[0])
      yield {"name" : compname, "url" : compurl, "price" : compprice, "ram" : compram, "storage": compstorage}

    next_pages = response.css('li.page-item.page-next a')
    yield from response.follow_all(next_pages, self.parse)
