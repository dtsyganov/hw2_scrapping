# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import hw2_scrapping.db as db

from hw2_scrapping.settings import ENGINE_URL
from sqlalchemy import create_engine

class SQLitePipeline:
    def __init__(self):
        self.db_engine = create_engine(ENGINE_URL, future=True)

    def open_spider(self, spider):
        self.conn = self.db_engine.connect()

    def close_spider(self, spider):
        self.conn.close()

    def process_item(self, item, spider):
        db.computer_insert(self.conn, item['url'], item['name'], item['ram'], item['storage'], item['price'])
        return item
