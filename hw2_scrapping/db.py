from sqlalchemy import (
    MetaData, Table, Column,
    Integer, Float, String, DateTime
)
from sqlalchemy import select, desc
from sqlalchemy.sql import func

'''Table computers {
  id int
  url varchar // ссылка на страницу товара
  visited_at timestamp //время посещения страницы

  name varchar // наименование товара
  cpu_hhz float // частота процессора, ГГЦ
  ram_gb int // объем ОЗУ, Гб
  ssd_gb int // Объем SSD, Гб
  price_rub int // Цена, руб

  rank float // вычисляемый рейтинг
}'''

meta = MetaData()

computers = Table(
    'computers', meta,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('url', String(200), nullable=False, unique=True),
    Column('visited_at', DateTime,  nullable=False, server_default=func.now()),
    Column('name', String(200), nullable=False, unique=False),
    Column('ram', Integer, nullable=False),
    Column('storage', Integer, nullable=False),
    Column('price', Integer, nullable=False),
    Column('rank', Float, nullable=False)
)

def init_tables(engine):
    meta.create_all(engine)

def computer_insert(conn, url: String(200), name: String(200), ram: Integer, storage: Integer, price: Float):
    try:
        rank = -0.005*price + 0.03*ram + 0.2*storage
        result = conn.execute(computers.insert(),
            { 'url': url, 'name': name, 'ram': ram, 'storage': storage, 'price': price, 'rank': rank } )
    except:
        conn.rollback()
    else:
        conn.commit()
        return result

def top_rank(conn, count: Integer):
# SELECT rank, name, ram, storage, price, url FROM computers ORDER BY field DESC LIMIT count;
    return conn.execute(select(
        computers.c.rank, computers.c.name, computers.c.ram, computers.c.storage, computers.c.price, computers.c.url
        ).order_by(desc(computers.c.rank)).limit(count))