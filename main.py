#! /usr/bin/python3

import os
from asyncio import run
from argparse import ArgumentParser
from hw2_scrapping.settings import DB_FILE, ENGINE_URL
from sqlalchemy import create_engine
import hw2_scrapping.db as db
import hw2_scrapping.spiders as spiders

import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

scrapy_engine = create_engine(ENGINE_URL, echo=True)

def drop_db():
    if os.path.exists(DB_FILE):
        os.remove(DB_FILE)

def init_db():
    drop_db()
    db.init_tables(scrapy_engine)

def top_rank():
    conn = scrapy_engine.connect()
    for cmp in db.top_rank(conn, 5):
        print(cmp)
    conn.close()

def scrap_sites():
    init_db()
    settings = get_project_settings()
    process = CrawlerProcess(settings)
    process.crawl(spiders.notik.NotikSpider)
    process.crawl(spiders.holodilnik.HolodilnikSpider)
    process.start() # the script will block here until all crawling jobs are finished

def work():
    scrap_sites()
    top_rank()

if __name__ == '__main__':
    parser = ArgumentParser(description='DB related shortcuts')
    parser.add_argument("-i", "--init",
                        help="Create empty database with tables",
                        action='store_true')
    parser.add_argument("-d", "--drop",
                        help="Drop database",
                        action='store_true')
    parser.add_argument("-t", "--top",
                        help="Top 10 by rank",
                        action='store_true')
    parser.add_argument("-s", "--scrap",
                        help="Scrap sites",
                        action='store_true')
    parser.add_argument("-w", "--work",
                        help="Work",
                        action='store_true')
    args = parser.parse_args()

    if args.init:
        init_db()
    elif args.drop:
        drop_db()
    elif args.top:
        top_rank()
    elif args.scrap:
        scrap_sites()
    elif args.work:
        work()
    else:
        work()
##        parser.print_help()
